import torch
from Config import unet_weights, gan_weights, dataset_dir, use_file, device
from PIL import Image
import torchvision
import matplotlib.pyplot as plt
from torchvision.transforms import ToPILImage, ToTensor
import random
import numpy as np
import os


to_tensor = ToTensor()
def interpolate(path, output_path, model_name):
    output_path = output_path + '.avi'
    import cv2
    from models.UNet import UNet
    from models.ConvNet import ConvNet
    def resize (im):
        return cv2.resize(im,(448, 256), interpolation=cv2.INTER_AREA)

    if model_name == 'unet':
        model = UNet().to(device)
        model.load_state_dict(torch.load(unet_weights))
        model.eval()
    elif model_name == 'gan':
        model = ConvNet().to(device)
        model.load_state_dict(torch.load(gan_weights))
        model.eval()


    cap = cv2.VideoCapture(path)
    #codec
    fourcc = cv2.VideoWriter_fourcc(*'XVID')
    out = cv2.VideoWriter(output_path, fourcc, cap.get(cv2.CAP_PROP_FPS) * 2, (448, 256))
    prior, act = None, None
    if (cap.isOpened()):
        ret, prior = cap.read()
        if not ret: return
        prior = to_tensor(resize(prior)).to(device)
    while ( cap.isOpened()):
        ret, akt = cap.read()
        if ret:
            akt = resize(akt)
            akt = to_tensor(akt).to(device)
            tmp = torch.cat((prior, akt), 0)
            predicted = model(torch.unsqueeze(tmp, 0))
            out.write((prior.to('cpu')*255).numpy().transpose(1,2,0).astype(np.uint8))
            out.write((torch.squeeze(predicted)*255).to('cpu').detach().numpy().transpose(1,2,0).astype(np.uint8))
            prior = akt
        else:
            break

    # Release everything if job is finished
    cap.release()
    out.release()
    cv2.destroyAllWindows()

if __name__ == '__main__':
    interpolate('../test.mp4')


