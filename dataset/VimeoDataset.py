import torch
from torch.utils.data import Dataset
import os
from PIL import Image
from numpy import asarray
from Config import dataset_dir, use_file
from torchvision.transforms import transforms as tf

mean = (0.5, 0.5, 0.5)
std = (0.5, 0.5, 0.5)
def denormalize(tensor):
    denormed = torch.rand((3, 256, 448))
    denormed[0] = tensor[0] * std[0] + mean[0]
    denormed[1] = tensor[1] * std[1] + mean[1]
    denormed[2] = tensor[2] * std[2] + mean[2]
    return denormed


class VimeoDataset(Dataset):
    def __init__(self, dataset_dir, use_file, transforms = []):
        self.data_dir = dataset_dir
        self.transforms = transforms
        # self.transforms.insert(0, tf.Normalize(mean, std))
        self.transforms.insert(0, tf.ToTensor())
        self.transforms = tf.Compose(self.transforms)

        self.items = []
        for line in open(use_file, 'r'):
            self.items.append(line.strip())

    # return size is (3, 256, 448)
    def __getitem__(self, item):
        item_dir = os.path.join(dataset_dir, self.items[item])
        names = list(map(lambda name: os.path.join(item_dir, name), sorted(os.listdir(item_dir))))
        item = self.transforms(Image.open(names[0])), self.transforms(Image.open(names[1])), self.transforms(Image.open(names[2]))
        # item = tuple(map(asarray, item))
        return item


    def __len__(self):
        return len(self.items)


# find mean and std to be used as hardcoded vals for normalization
# this was not necessary because as I have lately found out, the dataset is already scaled to the range 0-1
# but could be used for another datasets
def get_mean_std():
    import numpy as np
    d = VimeoDataset(dataset_dir, use_file)

    print('computing mean std')
    mean_a = 0
    mean_b = 0
    mean_c = 0
    mean_sq_a = 0
    mean_sq_b = 0
    mean_sq_c = 0
    cnt = 0
    for item in d:
        for picture in item:
            channel = picture[0]
            mean_a += channel.sum()
            mean_sq_a += (channel**2).sum()
            cnt += np.prod(channel.shape)

            channel = picture[1]
            mean_b += channel.sum()
            mean_sq_b += (channel ** 2).sum()
            cnt += np.prod(channel.shape)

            channel = picture[2]
            mean_c += channel.sum()
            mean_sq_c += (channel ** 2).sum()
            cnt += np.prod(channel.shape)

    mean_a = mean_a / cnt
    mean_b = mean_b / cnt
    mean_c = mean_c / cnt

    var_a = (mean_sq_a / cnt) - mean_a ** 2
    var_b = (mean_sq_b / cnt) - mean_b ** 2
    var_c = (mean_sq_c / cnt) - mean_c ** 2

    std_a = torch.std(var_a)
    std_b = torch.std(var_b)
    std_c = torch.std(var_c)

    print('mean')
    print(mean_a)
    print(mean_b)
    print(mean_c)
    print('std')
    print(std_a)
    print(std_b)
    print(std_c)


if __name__ == '__main__':
    get_mean_std()