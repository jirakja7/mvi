import torch
import torch.utils.data.dataloader as dl
from torch import optim
import os
from torch import nn
import torch.utils.tensorboard as tb
import torchvision
from dataset.VimeoDataset import VimeoDataset, denormalize
from Config import dataset_dir, use_file, unet_parameters, device, stats_dir, weights_dir
from datetime import datetime


name = 'unet_lr' + str(unet_parameters['lr']) + '_epoch_num' + str(unet_parameters['epoch_num']) + str(datetime.now())

def loss_f(out: torch.Tensor, target: torch.Tensor):
    criterion = nn.MSELoss()
    loss = criterion(out, target)
    return loss


cnt = 1
def trn(unet, optimizer, trn_loader, epoch, writer):
    unet.train()

    for batch_id, item in enumerate(trn_loader):
        item[0], item[1], item[2] = item[0].to(device), item[1].to(device), item[2].to(device)
        in_ = torch.cat((item[0], item[2]), 1)

        optimizer.zero_grad()

        out = unet(in_)
        loss = loss_f(out, item[1])
        loss.backward()
        optimizer.step()

        # log
        if batch_id % 100 == 0:
            global cnt
            print('EPOCH: ', epoch, ' BATCH: ', batch_id, ' REAL_LOSS: ', loss.item())
            writer.add_scalar('REAL_LOSS:', loss.item(), cnt)
            img_grid = torchvision.utils.make_grid([item[0][0], out[0], item[2][0]])
            writer.add_image('IMG_RESULTS', img_grid, cnt)
            cnt = cnt + 1

    torch.save(unet.state_dict(), os.path.join(weights_dir, name + str(epoch) + '.pt'))


def train_unet():
    # NET
    from models.UNet import UNet
    unet = UNet().to(device)
    # DATALOADER
    trn_set = VimeoDataset(dataset_dir, use_file)
    trn_loader = dl.DataLoader(trn_set, unet_parameters['batch_size'], shuffle=True, num_workers=6)
    # OPTIM
    optimizer = optim.Adam(unet.parameters(), lr=unet_parameters["lr"])

    writer = tb.SummaryWriter(os.path.join(stats_dir, name))

    for epoch in range(unet_parameters["epoch_num"]):
        trn(unet, optimizer, trn_loader, epoch, writer)

    print('Model was trained!!!')

if __name__ == '__main__':
    train_unet()