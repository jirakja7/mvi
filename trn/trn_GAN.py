import torch
import torch.utils.data.dataloader as dl
from torch import optim
from torch import nn
from dataset.VimeoDataset import VimeoDataset, denormalize
from Config import dataset_dir, use_file, gan_parameters, device, stats_dir, weights_dir
import torchvision
import os
import torch.utils.tensorboard as tb
from  datetime import time, datetime


name = 'gan_lr' + str(gan_parameters['lr_d']) + '_epoch_num' + str(gan_parameters['epoch_num']) + str(datetime.now())

def dreal_loss_f(dout : torch.Tensor):
        batch_size = dout.size(0)
        labels = torch.ones(batch_size).view((batch_size, -1)).to(device)
        criterion = nn.BCEWithLogitsLoss()
        loss = criterion(dout, labels)
        return loss


def dfake_loss_f(dout: torch.Tensor):
    batch_size = dout.size(0)
    labels = torch.zeros(batch_size).view((batch_size, -1)).to(device)
    criterion = nn.BCEWithLogitsLoss()
    loss = criterion(dout, labels)
    return loss

def g_loss_f(dout: torch.Tensor):
    batch_size = dout.size(0)
    labels = torch.ones(batch_size).view((batch_size,-1)).to(device)
    criterion = nn.BCEWithLogitsLoss()
    loss = criterion(dout, labels)
    return loss


def real_loss_f(g_out, label):
    criterion = nn.MSELoss()
    loss = criterion(g_out, label)
    return loss

cnt = 1
def trn(generator, discriminator, optimizer_g, optimizer_d, trn_loader, epoch, writer):
    generator.train()
    discriminator.train()

    for batch_id, item in enumerate(trn_loader):
        item[0], item[1], item[2] = item[0].to(device), item[1].to(device), item[2].to(device)
        target = item[1]
        in_g = torch.cat((item[0], item[2]), 1)
        in_d = torch.cat((item[0], item[1], item[2]), 1)
        # backprop discriminator
        optimizer_d.zero_grad()

        dreal = discriminator(in_d)
        dreal_loss = dreal_loss_f(dreal)

        out_g = generator(in_g)
        out_g = torch.cat((item[0], out_g, item[2]),1)
        dfake = discriminator(out_g)

        dfake_loss = dfake_loss_f(dfake)
        d_loss = dfake_loss + dreal_loss
        d_loss.backward()
        optimizer_d.step()

        # backprop generator
        optimizer_g.zero_grad()
        out_g = generator(in_g)
        real_loss = real_loss_f(out_g, item[1])
        out_g = torch.cat((item[0], out_g, item[2]), 1)
        dfake = discriminator(out_g)
        g_loss = g_loss_f(dfake) + real_loss
        g_loss.backward()
        optimizer_g.step()

        # log
        if batch_id % 100 == 0:
            global cnt
            real_loss = real_loss_f(out_g[:, 3:6, : , :], item[1])
            print('EPOCH: ', epoch, ' BATCH: ', batch_id, ' LOSS_G: ', g_loss.item(), ' LOSS_D: ' , d_loss.item(), ' REAL_LOSS: ', real_loss.item())
            writer.add_scalar('LOSS_G:', g_loss.item(), cnt)
            writer.add_scalar('LOSS_D:', d_loss.item(), cnt )
            writer.add_scalar('REAL_LOSS:', real_loss.item(), cnt )
            img_grid = torchvision.utils.make_grid([item[0][0], out_g[0, 3:6, : , :], item[2][0]])
            writer.add_image('IMG_RESULTS', img_grid, cnt )
            cnt = cnt + 1

        torch.save(generator.state_dict(), os.path.join(weights_dir, name + str(epoch) + '.pt') )
        torch.save(discriminator.state_dict(), os.path.join(weights_dir, name + str(epoch) + '_d.pt') )


def train_gan():
    # NET
    from models.ConvNet import ConvNet
    generator = ConvNet().to(device)
    from models.Discriminator import Discriminator
    discriminator = Discriminator().to(device)
    # DATALOADER
    trn_set = VimeoDataset(dataset_dir, use_file)
    trn_loader = dl.DataLoader(trn_set, gan_parameters['batch_size'], shuffle=True, num_workers=6)
    # OPTIM
    optimizer_g = optim.Adam(generator.parameters(), lr=gan_parameters["lr_g"])
    optimizer_d = optim.Adam(discriminator.parameters(), lr=gan_parameters["lr_d"])

    writer = tb.SummaryWriter(os.path.join(stats_dir, name))

    for epoch in range(gan_parameters["epoch_num"]):
        trn(generator, discriminator, optimizer_g, optimizer_d, trn_loader, epoch, writer)

    print('Gan was trained!!!')



if __name__ == '__main__':
    train_gan()