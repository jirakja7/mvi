This is a repository created for semestral work from ni-mvi. I've created tool for
interpolating video frame rate. 

Prerequisities:
* clone this rep
* create virtual enviroment for python and install requirements.txt 
(note that I've used python 3.7.3)
* in Config.py change root_dir to a directory where is your cloned
rep downloaded
#
Run:
* run python app.py --help for looking at correct way of running the
script
* EXAMPLE: python app.py test_video.mp4 interpolated unet
* run with your parameters
* enjoy :)


