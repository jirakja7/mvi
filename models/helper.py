from torch import nn
from Config import debug


def conv(in_channels, out_channels, kernel=3, stride=2, padding=1, batch_norm=True, activation_f=None):
    layers = []
    conv_layer = nn.Conv2d(in_channels, out_channels, kernel, stride, padding, bias=False)
    layers.append(conv_layer)
    if activation_f:
        layers.append(activation_f)
    if batch_norm:
        layers.append(nn.BatchNorm2d(out_channels))
    return nn.Sequential(*layers)


def debug(x):
    if debug:
        print(x)