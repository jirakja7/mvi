from torch import nn
from models.helper import conv
import torch
from Config import device

class Discriminator(nn.Module):
    def __init__(self):
        super(Discriminator, self).__init__()

        out1 = 24
        self.conv1 = conv(9, out1, activation_f=nn.ReLU())
        out2 = 48
        self.conv2 = conv(out1, out2, activation_f=nn.ReLU())
        out3 = 96
        self.conv3 = conv(out2, out3, activation_f=nn.ReLU())
        in_lin = 172032
        out_lin = 1024
        self.dense1 = nn.Linear(in_lin, out_lin)
        self.dense2 = nn.Linear(out_lin, 1)

        self.relu = nn.ReLU()

    def forward(self, x):
        x = self.conv1(x)
        x = self.conv2(x)
        x = self.conv3(x)
        x = x.view(x.size(0), -1)
        x = self.relu(self.dense1(x))
        x = self.relu(self.dense2(x))
        return x


if __name__ == '__main__':
    x = torch.rand((16, 9, 256, 448)).to(device)
    dis = Discriminator().to(device)
    out = dis(x)
    print(out.shape)
