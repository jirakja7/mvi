import torch
import torch.nn as nn
from models import helper
from models.helper import debug
from Config import device
from torchvision import transforms

def double_conv_f(in_channels, out_channels):
    conv1 = helper.conv(in_channels, out_channels, stride=1, padding=1, activation_f=nn.ReLU())
    conv2 = helper.conv(out_channels, out_channels, stride=1, padding=1, activation_f=nn.ReLU())
    return nn.Sequential(conv1, conv2)


class UNet(nn.Module):
    def __init__(self):
        super(UNet, self).__init__()

        self.max_pool = nn.MaxPool2d(2, 2)

        out1 = 64
        self.double_conv1 = double_conv_f(6, out1)
        out2 = 128
        self.double_conv2 = double_conv_f(out1, out2)
        out3 = 256
        self.double_conv3 = double_conv_f(out2, out3)
        out4 = 512
        self.double_conv4 = double_conv_f(out3, out4)
        out5 = 1024
        self.double_conv5 = double_conv_f(out4, out5)

        self.upsample1 = nn.ConvTranspose2d(out5, 512, 2, 2)
        self.double_conv6 = double_conv_f(1024, 512)
        self.upsample2 = nn.ConvTranspose2d(out4, 256, 2, 2)
        self.double_conv7 = double_conv_f(512, 256)
        self.upsample3 = nn.ConvTranspose2d(256, 128, 2, 2)
        self.double_conv8 = double_conv_f(256, 128)
        self.upsample4 = nn.ConvTranspose2d(128, 64, 2, 2)
        self.double_conv9 = double_conv_f(128, 64)

        self.final_conv = nn.Conv2d(64, 3, kernel_size=1, stride=1)
        self.final_activation = nn.ReLU()

    def forward(self, x):
        x = self.double_conv1(x)  # first
        cp4 = x.to('cpu')
        x = self.max_pool(x)
        x = self.double_conv2(x)  # second
        cp3 = x.to('cpu')
        x = self.max_pool(x)
        x = self.double_conv3(x)  # third
        cp2 = x.to('cpu')
        x = self.max_pool(x)
        x = self.double_conv4(x)  # fourth
        cp1 = x.to('cpu')
        x = self.max_pool(x)
        x = self.double_conv5(x)
        x = self.upsample1(x)

        x = torch.cat((x, cp1.to(device)), 1)
        x = self.double_conv6(x)
        x = self.upsample2(x)
        x = torch.cat((x, cp2.to(device)), 1)
        x = self.double_conv7(x)
        x = self.upsample3(x)
        x = torch.cat((x, cp3.to(device)), 1)
        x = self.double_conv8(x)
        x = self.upsample4(x)
        x = torch.cat((x, cp4.to(device)), 1)
        x = self.double_conv9(x)
        x = self.final_conv(x)
        x = self.final_activation(x)
        return x



if __name__ == "__main__":
    x = torch.rand((16,6,256, 448)).to(device)
    unet = UNet().to(device)
    out = unet(x)
    print(out.shape)
