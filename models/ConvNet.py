from torch import nn
from models.helper import conv
import torch
from Config import device

class ConvNet(nn.Module):
    def __init__(self):
        super(ConvNet, self).__init__()

        out1 = 48
        self.conv1 = conv(6, out1, stride=1)
        out2 = 24
        self.conv2 = conv(out1, out2, stride=1)
        out3 = 12
        self.conv3 = conv(out2, out3, stride=1)
        out4 = 12
        self.conv4 = conv(out3, out4, stride=1)
        out5 = 3
        self.conv5 = conv(out4, out5, stride=1)

        self.act_f = nn.Tanh()

    def forward(self, x):
        x = self.act_f(self.conv1(x))
        x = self.act_f(self.conv2(x))
        x = self.act_f(self.conv3(x))
        x = self.act_f(self.conv4(x))
        x = self.act_f(self.conv5(x))
        return x


if __name__ == '__main__':
    x = torch.rand((16,6,256, 448)).to(device)
    unet = ConvNet().to(device)
    out = unet(x)
    print(out.shape)
