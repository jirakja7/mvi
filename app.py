from argparse import ArgumentParser
from app.interpolate import interpolate

parser = ArgumentParser(description='Interpolate framerate of video, result is of the size 256x448')
parser.add_argument('video_path', help='path to video')
parser.add_argument('output_path', help='path of output, will be appended .avi format')
parser.add_argument('model', help='unet or gan')

args = parser.parse_args()
interpolate(args.video_path, args.output_path, args.model)
