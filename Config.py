root_dir = '/home/honzov/mvi'
stats_dir = root_dir + '/stats'
weights_dir = root_dir + '/weights'
dataset_dir = root_dir + '/vimeo_triplet/sequences'
use_file = root_dir + '/vimeo_triplet/tri_trainlist.txt'

gan_parameters ={"batch_size": 16, "lr_g": 1e-3, "lr_d": 1e-3, "epoch_num": 200}
unet_parameters = {"batch_size": 8, "lr": 1e-3, "epoch_num": 200 }

gan_weights = root_dir + '/weights/gan_final_w.pt'

unet_weights = root_dir + '/weights/unet_final_w.pt'

debug = True

device = "cuda"